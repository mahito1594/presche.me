---
title: "List of Talks"
date: 2020-04-09T01:35:00+09:00
timestamp: "/data/talks.yml"
katex: true
draft: false
---

講演リストのページです．
各項目は"タイトル・研究集会・大学(研究機関など研究集会が行われた場所)・市名・国名・日付"の順に並んでいます．

{{< listTalks "talks" >}}
