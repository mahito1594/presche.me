---
title: "Mahito Tanno"
date: 2020-04-08T19:51:27+09:00
draft: false
---

## About
I am a programmer working in Tokyo, Japan.
I studied mathematics (my speciality was algebraic geometry).
Especially, I was interested in relations between singularities and number-theoristic invariants.

### Social Accounts

- ORCiD: [0000-0002-1377-6487](https://orcid.org/0000-0002-1377-6487)
- GitHub: [mahito1594](https://github.com/mahito1594)
- GitLab: [mahito1594](https://gitlab.com/mahito1594)
- Akkoma: [@mahito@ringed.space](https://ringed.space/mahito)

## Contact
Please replace `[at]` with `@`.

- Email: `mahito[at]presche.me`
- Email: `pb94.mahito[at]gmail.com`

## Certification
- Salesforce Certified Platform App Builder

## Education

- 2017: B.S. in mathematics, Department of Mathematics, Osaka University
- 2019: M.S. in mathematics, Department of Mathematics, Osaka University
- 2022: Ph.D. in mathematics , Department of Mathematics, Osaka University
