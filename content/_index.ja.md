---
title: "丹野 真人"
date: 2020-04-08T19:51:27+09:00
draft: false
---

東京でプログラマをしています．

大学では数学を勉強していました．
専門は代数幾何学で，特異点論に興味を持って研究していました．

### Social Accounts

- ORCiD: [0000-0002-1377-6487](https://orcid.org/0000-0002-1377-6487)
- GitHub: [mahito1594](https://github.com/mahito1594)
- GitLab: [mahito1594](https://gitlab.com/mahito1594)

## Contact
`[at]` を `@` に置き換えてください

- Email: `mahito[at]presche.me`
- Email: `pb94.mahito[at]gmail.com`

## 資格
- Salesforce Certified Platform App Builder

## 学歴

- 2017: 学士（理学），大阪大学理学部数学科
- 2019: 修士（理学），大阪大学大学院理学研究科数学専攻
- 2022: 博士（理学），大阪大学大学院理学研究科数学専攻
