---
title: "List of Talks"
date: 2020-04-09T01:35:00+09:00
timestamp: "/data/talks.yml"
katex: true
draft: false
---

Here I put a list of talks at seminars, conferences and so on.
The information is given in the form: title, seminar (or conference), the institute where the talk is given, city, country, date.

{{< listTalks "talks" >}}
